var laRulet = new Winwheel({
    'numSegments': 12,
    'outerRadius': 290,
    'lineWidth': 2,
    'segments': [
        {'fillStyle':'#4adb82', 'text':'Dau', 'id':'dau'},
        {'fillStyle':'#f0e768', 'text':'Pregunta', 'id':'pregunta'},
        {'fillStyle':'#f06895', 'text':'Repte', 'id':'repte'},
        {'fillStyle':'#f0e768', 'text':'Pregunta', 'id':'pregunta'},
        {'fillStyle':'#4adb82', 'text':'Dau', 'id':'dau'},
        {'fillStyle':'#f0e768', 'text':'Pregunta', 'id':'pregunta'},
        {'fillStyle':'#f06895', 'text':'Repte', 'id':'repte'},
        {'fillStyle':'#f0e768', 'text':'Pregunta', 'id':'pregunta'},
        {'fillStyle':'#4adb82', 'text':'Dau', 'id':'dau'},
        {'fillStyle':'#f0e768', 'text':'Pregunta', 'id':'pregunta'},
        {'fillStyle':'#f06895', 'text':'Repte', 'id':'repte'},
        {'fillStyle':'#f0e768', 'text':'Pregunta', 'id':'pregunta'},
    ],
    'animation':{
        'type': 'spinToStop',
        'duration': 3,
        'callbackFinished': 'missatge()',
        'callbackAfter': 'drawIndicador()'
    }
});

var equips = [];
var torn = 0;
var ronda = 1;
var preguntes = ['Pregunta 1', 'Pregunta 2', 'Pregunta 3', 'Pregunta 4', 'Pregunta 5', 'Pregunta 6'];
var preguntesHanSortit = [];

drawIndicador();

function drawIndicador() {
    var ctx = laRulet.ctx;
    ctx.strokeStyle = 'black';
    ctx.fillStyle = 'royalblue';
    ctx.lineWidth = 4;
    ctx.beginPath();
    ctx.moveTo(300, 0);
    ctx.lineTo(310, 0);
    ctx.lineTo(300, 40);
    ctx.lineTo(291, 0);
    ctx.stroke();
    ctx.fill();
}

function missatge() {
    var selSegment = laRulet.getIndicatedSegment();
    laRulet.stopAnimation(false);
    laRulet.rotationAngle = 0;
    if(selSegment.id==="dau"){
        opcioDau();
    }else if(selSegment.id==="pregunta"){
        opcioPregunta();
    }else if(selSegment.id==="repte"){
        opcioRepte();
    }
}

function opcioDau(){
    openModal("Gira el dau...","<div class=\"dauRotador\" onClick=\"moureDau()\"></div><input id=\"resDau\" type=\"hidden\" value=\"\" /> ", [['Continuar','dauContinue()']])
}

function opcioPregunta(){
    openModal("Respon la pregunta!", getPregunta()+"<br/><br/>Ha contestat correctament?", [['Si','fiTorn(1)'],['No','fiTorn(0)']])
}

function getPregunta(){
    if(preguntesHanSortit.length < preguntes.length){
        do{
            laPreg=Math.floor(Math.random() * ((preguntes.length-1) - 0 + 1)) + 0;
        } while (preguntesHanSortit.includes(laPreg));
        preguntesHanSortit.push(laPreg);
    }else{
        laPreg=Math.floor(Math.random() * ((preguntes.length-1) - 0 + 1)) + 0;
        preguntes=[];
        
    }
    return preguntes[laPreg];
}

function opcioRepte(){
    openModal("Prepara't, que toca un repte!","Quants punts han guanyat?<br/><input id=\"numberIn\" type=\"number\" min=\"0\" value=\"0\"/>", [['Continuar','donarPuntscustom()']])
}

function donarPuntscustom(){
    fiTorn(parseInt($("#numberIn").val()));
}

function fiTorn(punts){
    donarPunts(torn,punts);
    nextTorn();
    hideModal();
}

function startGame(){
    if($("#numberIn").val() >1 && $("#numberIn").val() < 8){
        for (let i = 1; i <= $("#numberIn").val(); i++) {
            equips.push(i);
        }
        html="";
        equips.forEach(function(e){
            html+="<div id=\"grup-"+e+"\" class=\"grupCard\">Equip "+e+"<div id=\"punts-"+e+"\" class=\"punts\">0</div></div>";
        });
        $(".grupsParticipants").html(html);
        nextTorn();
        nextRonda();
        hideModal();
    }else{
        $("#avisModal").fadeIn(0);
    }
}

function hideModal(){
    $(".modalHolder").fadeOut(100);
}

function openModal(titol, text, botons){
    $(".modalTitol").html(titol);
    $(".modalText").html(text);
    buttonsHtml = "";
    botons.forEach(function(e){
        buttonsHtml+="<button id=\"modalButton\" onclick=\""+e[1]+"\"> "+e[0]+" </button>";
    });
    $(".modalButtons").html(buttonsHtml);
    $(".modalHolder").fadeIn(100);
}

function changeModalContent(titol, text, botons){
    $(".modalTitol").fadeOut(100);
    $(".modalText").fadeOut(100);
    $(".modalButtons").fadeOut(100);
    setTimeout(function(){
        openModal(titol, text, botons);
        setTimeout(function(){
            $(".modalTitol").fadeIn(100);
            $(".modalText").fadeIn(100);
            $(".modalButtons").fadeIn(100);
        }, 200);
    }, 100);
}

function nextTorn(){
    torn+=1;
    if(torn>equips.length){ 
        torn=1;
        nextRonda();
    }
    $("#grupTit").html(torn);
    $(".grupCard").removeClass("active")
    $("#grup-"+torn).addClass("active");
}

function donarPunts(torn, punts){
    $("#punts-"+torn).html(parseInt($("#punts-"+torn).html())+parseInt(punts))
}

function moureDau(){
    if(!$(".dauRotador").hasClass("rotant")){
        $(".dauRotador").addClass("rotant");
        temps=0;
        lastNum=0;
        while(temps<6000){
            setTimeout(function(){
                $(".dauRotador").removeClass("d1 d2 d3 d4 d5 d6");
                do{
                    elNum=Math.floor(Math.random() * (6 - 1 + 1)) + 1;
                }while(elNum==lastNum)
                $(".dauRotador").addClass("d"+elNum);
                $("#resDau").val(elNum);
            }, temps);
            temps+=200;
        }
        setTimeout(function(){
            $(".dauRotador").removeClass("rotant");
        }, 8000);
    }
}

function dauContinue(){
    if($("#resDau").val()==""){
        $(".dauRotador").addClass("rotant");
        setTimeout(function(){
            $(".dauRotador").removeClass("rotant");
        }, 1000);
    }else if(!$(".dauRotador").hasClass("rotant")){
        selEquips="<select id=\"selEquips\">";
        equips.forEach(function(e){
            if(e!=torn){
                selEquips+="<option value=\""+e+"\">Equip "+e+"</option>";
            }
        });
        selEquips+="</select>"
        if($("#resDau").val()==1 || $("#resDau").val()==4){
            changeModalContent("El 1 o el 4: Guerra!", "Trieu un equip contra el que vulgueu competir...<br/>"+selEquips+"<br/>Quants punts ha guanyat l'Equip "+torn+": <input id=\"guerTorn\" class=\"guerrIn\" type=\"number\" value=\"0\" min=\"0\" /><br/>Quants punts ha guanyat l'oponent: <input id=\"guerOpo\"  class=\"guerrIn\" type=\"number\" value=\"0\" min=\"0\" /><br/><br/><br/><br/><br/>", [['Continuar','fiTornGuerra()']]);
        }else if($("#resDau").val()==2 || $("#resDau").val()==5){
            changeModalContent("El 2 o el 5: Pregunta x2!", "Us ha tocat la la pregunta:</br>"+getPregunta()+"<br/><br/>Ha contestat correctament?", [['Si','fiTorn(2)'],['No','fiTorn(0)']]);
        }else{
            changeModalContent("El 3 o el 6: Robart punts!", "Robar punt! Tria un equip al que robareu punts...<br/>"+selEquips+"<br/>Quants punts ha robat l'Equip "+torn+": <input id=\"robTorn\" class=\"guerrIn\" type=\"number\" value=\"0\" min=\"0\" /><br/><br/><br/><br/><br/>", [['Continuar','fiTornRobar()']]);
        }
    }
}

function fiTornGuerra(){
    donarPunts($("#selEquips").val(),$("#guerOpo").val());
    fiTorn($("#guerTorn").val());
}

function fiTornRobar(){
    donarPunts($("#selEquips").val(),"-"+$("#robTorn").val());
    fiTorn($("#robTorn").val());
}

function acabarPartida(){
    winTeamScore=-9990;
    winTeam=0;
    equips.forEach(function(e){
        if($("#punts-"+e).html() > winTeamScore){
            winTeamScore=$("#punts-"+e).html();
            winTeam=e;
        }
    })
    openModal("Partida finalitzada!", "Felicitats equip "+winTeam+" has guanyat la partida!", [['Reiniciar','resetJoc()']]);
}

function resetJoc(){
    location.reload();
}

function nextRonda(){
    $("#rondCont").html(parseInt($("#rondCont").html())+1);
}
